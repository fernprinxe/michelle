# michelle

[![michelle](https://img.shields.io/badge/license-BSD3--clause%20license-brightgreen)](https://en.wikipedia.org/wiki/BSD_licenses#3-clause_license_(%22BSD_License_2.0%22,_%22Revised_BSD_License%22,_%22New_BSD_License%22,_or_%22Modified_BSD_License%22))

## server

```
apt install git gcc g++ make python  
git clone https://gitlab.com/fernprinxe/michelle.git  
cd ./michelle/server && make  
chmod +x michelle  
./michelle  
```

## usage

-h, --help            show this help message and exit  
-v, --verbose         verbose output  
-c, --config=<str>    config file to use  
-d, --web-dir=<str>   location of the web directory  
-b, --bind=<str>      bind to address  
-p, --port=<int>      listen on port   

## client

```
apt install python
curl -o /usr/local/bin https://gitlab.com/fernprinxe/michelle/-/raw/master/client/michelle.py
curl -o /lib/systemd/system https://gitlab.com/fernprinxe/michelle/-/raw/master/client/michelle.service
vi /usr/local/bin/michelle.py
chmod +x /usr/local/bin/michelle.py
systemctl enable michelle
systemctl start michelle
systemctl status michelle
```

## from

BotoX: https://github.com/BotoX/ServerStatus  
CokeMine: https://github.com/CokeMine/ServerStatus-Hotaru